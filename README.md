# bonsaicode/session
Session handler

* Start a session with a given timeout.
* Regenerating the session id (e.g. after authentication).
* Remove the session.

This attempts to preserve the session on an unstable network connection (which may occur on mobile phones) when starting the session and when regenerating the session id.

# Requirements
* PHP 7.1 or higher

# Installation
To install the module, run the following command in the console:

`$ composer require "bonsaicode/session"`

# Configure
To adjust the default session timeout, edit `Session::$TIMEOUT`.

# Usage

	<?php
		# at the top of the login page and in your logout page:
		BonsaiCode\Session::remove();

		# at the top of all pages with session:
		BonsaiCode\Session::start();

		# after logging in, prevent session fixation by running this:
		BonsaiCode\Session::regenerateId();
	?>
